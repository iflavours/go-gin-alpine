Golang + Gin-Gonic/Gin on Alpine Linux
===============================

This repository contains a Golang + Gin-Gonic/Gin, published to the public Docker Hub via automated build mechanism.

Usage
-----

Please notice this image is configured with a workdir `/code`, so make sure you mount such volume first.

### General usage

```bash
$ docker run -v $(pwd):/code iflavoursbv/go-gin-alpine:latest 'your-commands-here'
```


Configuration
-------------
This docker image is based on the following stack:
- OS: Alpine Linux
- Golang: Go 1.9.2
- gin-gonic/gin

Dependencies
------------

- [golang:1.9-alpine](https://hub.docker.com/_/golang/)

History
-------
* 0.1.0 - Initial version
* 0.2.0 - Updated to Go 1.9.0
* 0.3.0 - Updated to Go 1.9.2

License
-------

[Licensed under MIT License](https://bitbucket.org/iflavours/go-gin-alpine/raw/master/LICENSE)

